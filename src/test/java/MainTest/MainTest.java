package MainTest;

import dao.DAO;
import dao.DonHangDAO;
import model.DonHang;
import org.junit.Test;

import java.sql.*;
import java.util.List;

import static org.junit.Assert.assertTrue;


public class MainTest {

    @Test
    public void testOK() throws SQLException {
        // them data test
        initData();

        // expected
        String maDH = "DH01";

        // actual
        DonHangDAO donHangDAO = new DonHangDAO();
        List<DonHang> donHangList = donHangDAO.findAllById(maDH);

        // check
        // xem ds tìm được có giống mong muốn hay không
        assertTrue(donHangList.get(0).getId().contains(maDH));

        // revert data
        String deleteStatement = "delete from tbl_don_hang where id = ?";
        DAO dao = new DAO();
        // data don hang
        PreparedStatement preparedStmt = dao.con.prepareStatement(deleteStatement);
        preparedStmt.setString(1, "DH01");
        preparedStmt.execute();
        // data khach hang
        deleteStatement = "delete from tbl_khach_hang where id = ?";
        preparedStmt = dao.con.prepareStatement(deleteStatement);
        preparedStmt.setString(1, "KH01");
        preparedStmt.execute();
    }

    @Test
    public void testNotFound() throws SQLException {
        // them data test
        initData();

        // expected
        String maDH = "DH02";

        // actual
        DonHangDAO donHangDAO = new DonHangDAO();
        List<DonHang> donHangList = donHangDAO.findAllById(maDH);

        // check
        // xem ds tìm được có giống mong muốn hay không
        assertTrue(donHangList.isEmpty());

        // revert data
        String deleteStatement = "delete from tbl_don_hang where id = ?";
        DAO dao = new DAO();
        // data don hang
        PreparedStatement preparedStmt = dao.con.prepareStatement(deleteStatement);
        preparedStmt.setString(1, "DH01");
        preparedStmt.execute();
        // data khach hang
        deleteStatement = "delete from tbl_khach_hang where id = ?";
        preparedStmt = dao.con.prepareStatement(deleteStatement);
        preparedStmt.setString(1, "KH01");
        preparedStmt.execute();
    }


    public void initData() throws SQLException {
        DAO dao = new DAO();
        PreparedStatement statement = null;
        // insert DH
        String insertDH = " insert into tbl_don_hang (id, createdDate, status, note, totalPrice)"
                + " values (?, ?, ?, ?, ?)";
        statement = dao.con.prepareStatement(insertDH);
        statement.setString(1, "DH01");
        statement.setDate(2, new Date(System.currentTimeMillis()));
        statement.setString(3, "Đã tạo");
        statement.setString(4, "note");
        statement.setFloat(5, 5000f);
        statement.execute();


        // insert KH
        String insertKH = " insert into tbl_khach_hang (id, name)"
                + " values (?, ?)";
        statement = dao.con.prepareStatement(insertKH);
        statement.setString(1, "KH01");
        statement.setString(2, "Trần Dũng");
    }
}
