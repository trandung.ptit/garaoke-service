<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="dao.DAO" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Garaoke</title>
    <link rel="stylesheet" href="css/bulma.min.css">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <div class="columns has-background-white-ter" style="margin: 0;padding: 0">
        <div class="column is-one-fifth" style="width: 230px;border-right:solid #c5c6c7 1px;height: 100vh;background-color: #001f2b;color:#ffffff">
            <aside class="menu">
                <a href="gdChinh.jsp"><p style="font-size: 25px; font-weight: bold">GARAOKE</p></a>
                <p style="font-size: 20px">Danh mục</p>
                <ul class="menu-list">
                    <li class="pl-2"><a>Nhân viên</a></li>
                    <li class="pl-2"><a>Khách hàng</a></li>
                    <li class="pl-2"><a>Hoá đơn</a></li>
                    <li class="pl-2"><a>Kho hàng</a></li>
                    <li class="pl-2"><a>Dịch vụ</a></li>
                </ul>
                <p style="font-size: 20px">Tiện ích</p>
                <ul class="menu-list">
                    <li class="pl-2"><a>Sửa chữa</a></li>
                    <li class="pl-2"><a href="gdTimDonHang.jsp">Xuất kho</a></li>
                </ul>
            </aside>
        </div>
        <div class="column">
            <div class="columns is-centered">
                <div class="column is-half">
                    <img src="img/background.svg" alt="">
                </div>
            </div>
        </div>
    </div>
</body>
</html>
