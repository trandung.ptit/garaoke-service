<%@ page import="dao.DonHangDAO" %><%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 24/11/2021
  Time: 3:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Lưu trạng thái</title>
    <script src="js/sweetalert2@11.js"></script>
</head>
<body>
    <%
        String idDonHang = request.getParameter("idDonHang");
        DonHangDAO donHangDAO = new DonHangDAO();
        boolean kq = donHangDAO.saveStatus(idDonHang);
        if (kq){
    %>
    <script>
        Swal.fire({
            title : 'Thành công',
            text: 'Đơn hàng đã được xuất kho thành công',
            icon: 'success',
            confirmButtonColor: '#3085d6',
        }).then(() =>{
            window.location = "gdChinh.jsp";
        });
    </script>
    <%
        } else {
    %>
    <script>
        Swal.fire({
            title : 'Thất bại',
            text: 'Xuất kho thất bại',
            icon: 'error',
            showCancelButton: true,
            showConfirmButton: false,
            cancelButtonText: 'OK'
        }).then(() =>{
            window.location = "gdChinh.jsp";
        });
    </script>
    <%
        }
    %>
</body>
</html>
