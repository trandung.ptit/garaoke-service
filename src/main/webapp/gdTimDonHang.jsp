<%@ page import="model.DonHang" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.util.Random" %>
<%@ page import="model.LinhKienDonHang" %>
<%@ page import="model.LinhKien" %>
<%@ page import="model.KhachHang" %>
<%@ page import="dao.DonHangDAO" %><%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 13/11/2021
  Time: 1:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Đơn hàng</title>
    <link rel="stylesheet" href="css/bulma.min.css">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
<div class="columns has-background-white-ter" style="margin: 0;padding: 0">
    <div class="column is-one-fifth" style="width: 230px;border-right:solid #c5c6c7 1px;background-color: #001f2b;color:#ffffff;min-height: 100vh">
        <aside class="menu">
            <a href="gdChinh.jsp"><p style="font-size: 25px; font-weight: bold">GARAOKE</p></a>
            <p style="font-size: 20px">Danh mục</p>
            <ul class="menu-list">
                <li class="pl-2"><a>Nhân viên</a></li>
                <li class="pl-2"><a>Khách hàng</a></li>
                <li class="pl-2"><a>Hoá đơn</a></li>
                <li class="pl-2"><a>Kho hàng</a></li>
                <li class="pl-2"><a>Dịch vụ</a></li>
            </ul>
            <p style="font-size: 20px">Tiện ích</p>
            <ul class="menu-list">
                <li class="pl-2"><a>Sửa chữa</a></li>
                <li class="pl-2"><a href="gdTimDonHang.jsp">Xuất kho</a></li>
            </ul>
        </aside>
    </div>
    <div class="column">
        <div class="container is-fluid mt-1 p-1 pt-4">
            <div class="columns is-centered">
                <div class="column box">
                    <form class="columns is-three-quarters p-4" action="gdTimDonHang.jsp" method="post">
                        <input id="idDonHang" name="idDonHang" class="input column" type="text"
                               placeholder="Nhập mã đơn hàng">
                        <input type="submit" class="button is-info column is-1" value="Tìm kiếm"></input>
                    </form>
                    <div class="p-4">
                        <%
                            String idDonHang = request.getParameter("idDonHang");

                            if (idDonHang!=null){
                                DonHangDAO donHangDAO = new DonHangDAO();
                                List<DonHang> donHangList = donHangDAO.findAllById(idDonHang);
                        %>
                        <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                                <thead>
                                <tr>
                                    <th class="has-text-left" style="width: 120px">Mã đơn hàng</th>
                                    <th class="has-text-centered" style="width: 120px">Ngày tạo</th>
                                    <th class="has-text-left" style="width: 250px">Khách hàng</th>
                                    <th class="has-text-right" style="width: 50px">Tổng tiền</th>
                                    <th class="has-text-left">Ghi chú</th>
                                    <th class="has-text-left" style="width: 200px">Trạng thái</th>
                                    <th class="has-text-left" style="width: 50px">Lựa chọn</th>
                                </tr>
                                </thead>
                                <tbody>
                                <%
                                    for (DonHang donHang : donHangList){
                                        String isValidStatus = !"Đã tạo".equals(donHang.getStatus()) ? "disabled" : "";
                                        System.out.println(isValidStatus);
                                        %>
                                <tr>
                                    <td class="has-text-left"> <%=donHang.getId()%></td>
                                    <td class="has-text-centered"> <%=new SimpleDateFormat("dd/MM/yyyy").format(donHang.getCreatedDate())%></td>
                                    <td class="has-text-left"> <%=donHang.getKhachHang().getName()%></td>
                                    <td class="has-text-right"> <%=new DecimalFormat("###,###,###").format(donHang.getTotalPrice())%></td>
                                    <td class="has-text-left"> <%=donHang.getNote()%></td>
                                    <td class="has-text-left"> <%=donHang.getStatus()%></td>
                                    <td><a <%=isValidStatus%> class="button" href="gdDSLinhKien.jsp?idDonHang=<%=donHang.getId()%>" style="background-color: #dbdbdb" >Chọn</a></td>
                                </tr>
                                <%}%>
                                </tbody>
                            </table>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
