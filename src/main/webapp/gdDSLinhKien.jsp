<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 13/11/2021
  Time: 3:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="dao.DAO" %>
<%@ page import="model.LinhKienDonHang" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="model.LinhKien" %>
<%@ page import="java.util.Random" %>
<%@ page import="dao.LinhKienDonHangDAO" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Garaoke</title>
    <link rel="stylesheet" href="css/bulma.min.css"/>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<div class="columns has-background-white-ter" style="margin: 0;padding: 0">
    <div class="column is-one-fifth" style="width: 230px;border-right:solid #c5c6c7 1px;height: 100vh;background-color: #001f2b;color:#ffffff">
        <aside class="menu">
            <a href="gdChinh.jsp"><p style="font-size: 25px; font-weight: bold">GARAOKE</p></a>
            <p style="font-size: 20px">Danh mục</p>
            <ul class="menu-list">
                <li class="pl-2"><a>Nhân viên</a></li>
                <li class="pl-2"><a>Khách hàng</a></li>
                <li class="pl-2"><a>Hoá đơn</a></li>
                <li class="pl-2"><a>Kho hàng</a></li>
                <li class="pl-2"><a>Dịch vụ</a></li>
            </ul>
            <p style="font-size: 20px">Tiện ích</p>
            <ul class="menu-list">
                <li class="pl-2"><a>Sửa chữa</a></li>
                <li class="pl-2"><a href="gdTimDonHang.jsp">Xuất kho</a></li>
            </ul>
        </aside>
    </div>
    <div class="column">
        <div class="container is-fluid mt-1 p-1 pt-4">
            <div class="columns is-centered">
                <div class="column">
                    <div class="p-4">
                        <%
                            String idDonHang = request.getParameter("idDonHang");
                            List<LinhKienDonHang> linhKienDonHangs;
                            if (idDonHang!=null){
                                LinhKienDonHangDAO lkdhDAO = new LinhKienDonHangDAO();
                                linhKienDonHangs = lkdhDAO.findByOrderId(idDonHang);
                                int tong = 0;
                                for (LinhKienDonHang lk: linhKienDonHangs){
                                    tong += lk.getQuantity();
                                }
                        %>
                        <div class="box">
                            <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                                <thead>
                                <tr>
                                    <th class="has-text-left" style="width: 120px">Mã linh kiện</th>
                                    <th class="has-text-left" style="width: 250px">Tên linh kiện</th>
                                    <th class="has-text-left">Mô tả</th>
                                    <th class="has-text-right" style="width: 120px">Số lượng (Tổng: <%=tong%>)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <%
                                    for (LinhKienDonHang i : linhKienDonHangs){ %>
                                <tr>
                                    <td class="has-text-left"> <%=i.getLinhKien().getId()%></td>
                                    <td class="has-text-left"> <%=i.getLinhKien().getName()%></td>
                                    <td class="has-text-left"> <%=i.getLinhKien().getDescription()%></td>
                                    <td class="has-text-right"> <%=i.getQuantity()%></td>
                                </tr>
                                <%}%>
                                </tbody>
                            </table>
                        </div>
                        <form action="doLuuTrangThai.jsp" method="post">
                            <input name="idDonHang" hidden value="<%=idDonHang%>">
                            <input type="submit" class="button is-info is-pulled-right" value="Đã giao"/>
                        </form>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

