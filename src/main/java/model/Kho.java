package model;

import lombok.Data;

@Data
public class Kho {
    private String id;
    private String name;
    private String description;
    private String address;
}
