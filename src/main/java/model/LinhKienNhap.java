package model;

import lombok.Data;

@Data
public class LinhKienNhap {
    private String id;
    private Integer quantity;
    private Float price;
    private LinhKien linhKien;
    private PhieuNhap phieuNhap;
}
