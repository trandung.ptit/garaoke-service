package model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
public class LinhKienDonHang {
    private String id;
    private Integer quantity;
    private Float price;
    private Float totalPrice;
    private LinhKien linhKien;
}
