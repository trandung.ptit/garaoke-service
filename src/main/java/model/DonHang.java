package model;

import lombok.Data;

import java.util.Date;
@Data
public class DonHang {
    private String id;
    private Date createdDate;
    private String status;
    private Float totalPrice;
    private String note;
    private Xe xe;
    private KhachHang khachHang;
    private NhanVien phuTrachKyThuat;
    private NhanVien nvKho;
    private NhanVien nvKeToan;
}
