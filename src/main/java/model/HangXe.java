package model;

import lombok.Data;

@Data
public class HangXe {
    private String id;
    private String name;
    private String description;
}
