package model;

import lombok.Data;

@Data
public class Xe {
    private String id;
    private String description;
    private DongXe dongXe;
    private HangXe hangXe;
}
