package model;

import lombok.Data;

@Data
public class Cho {
    private String id;
    private String status;
    private NhanVien nvKyThuat;
    private Xe xe;
}
