package model;

import lombok.Data;

@Data
public class NhaCungCap {
    private String id;
    private String name;
    private String description;
}
