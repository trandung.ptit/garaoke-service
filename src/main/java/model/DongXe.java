package model;

import lombok.Data;

@Data
public class DongXe {
    private String id;
    private String name;
    private String description;
}
