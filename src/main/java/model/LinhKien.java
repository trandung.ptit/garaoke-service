package model;

import lombok.Data;

@Data
public class LinhKien {
    private String id;
    private String name;
    private String description;
    private Float price;
}
