package model;

import lombok.Data;

@Data
public class KhachHang {
    private String id;
    private String name;
    private String phone;
    private String address;
    private String note;
}
