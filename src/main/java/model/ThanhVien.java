package model;

import lombok.Data;

@Data
public class ThanhVien {
    private String id;
    private String name;
    private String username;
    private String password;
    private String phone;
}
