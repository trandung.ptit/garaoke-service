package model;

import lombok.Data;

import java.util.Date;

@Data
public class PhieuNhap {
    private String id;
    private Date createdDate;
    private Float totalPrice;
    private Kho kho;
    private NhaCungCap nhaCC;
    private NhanVien nvKho;
}
