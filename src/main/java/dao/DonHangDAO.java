package dao;

import model.DonHang;
import model.KhachHang;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DonHangDAO extends DAO {
    public DonHangDAO() {
        super();
    }

    public List<DonHang> findAllById(String id) {
        List<DonHang> donHangList = new ArrayList<>();
        String query = "SELECT dh.id,dh.createdDate,dh.status,dh.note,dh.totalPrice,kh.name\n" +
                "FROM tbl_don_hang dh\n" +
                "LEFT JOIN tbl_khach_hang kh on dh.tblKhachHangId = kh.id\n" +
                "WHERE dh.id LIKE '%" + id + "%'";
        try (
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query)
        ) {
            while(rs.next()){
                //DonHang
                DonHang donHang = new DonHang();
                donHang.setId(rs.getString("id"));
                donHang.setCreatedDate(rs.getDate("createdDate"));
                donHang.setStatus(rs.getString("status"));
                donHang.setNote(rs.getString("note"));
                donHang.setTotalPrice(rs.getFloat("totalPrice"));
                //KhachHang
                KhachHang khachHang = new KhachHang();
                khachHang.setName(rs.getString("name"));
                donHang.setKhachHang(khachHang);

                donHangList.add(donHang);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return donHangList;
    }

    public boolean saveStatus(String id){
        String sql = "update tbl_don_hang\n" +
                "set status = 'Đã giao'\n" +
                "where id = '" + id + "'";
        System.out.println(sql);
        try {
            Statement stmt = con.createStatement();
            stmt.executeUpdate(sql);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
