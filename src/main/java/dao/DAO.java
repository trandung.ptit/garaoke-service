package dao;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAO {
    public static Connection con;

    public DAO() {
        if(con == null){
            String dbUrl = "";
            String dbClass = "";
            String env = System.getenv("APP_ENV");
            System.out.println(env);

            // neu moi truong la test thi chay db nay
            if ("test".equals(env)){
                dbUrl = "jdbc:h2:mem:test;MODE=MYSQL";
                dbClass = "org.h2.Driver";
                //Initialize the script runner
                try {
                    Class.forName(dbClass);
                    con = DriverManager.getConnection (dbUrl, "root", "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ScriptRunner sr = new ScriptRunner(con);
                //Creating a reader object
                Reader reader = null;
                try {
                    reader = new BufferedReader(new FileReader("src/main/resources/create_table.sql"));
                    System.out.println(reader);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                //Running the script
                sr.setAutoCommit(true);
                sr.runScript(reader);
            } else{
                dbUrl = "jdbc:mysql://localhost:3306/gara?autoReconnect=true&useSSL=false&createDatabaseIfNotExist=true";
                dbClass = "com.mysql.cj.jdbc.Driver";
                try {
                    Class.forName(dbClass);
                    con = DriverManager.getConnection (dbUrl, "root", "");
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
