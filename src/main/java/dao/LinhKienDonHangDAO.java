package dao;

import model.DonHang;
import model.KhachHang;
import model.LinhKien;
import model.LinhKienDonHang;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LinhKienDonHangDAO extends DAO{
    public LinhKienDonHangDAO() {
        super();
    }

    public List<LinhKienDonHang> findByOrderId(String id) {
        List<LinhKienDonHang> lkdhs = new ArrayList<>();
        String query = "select lk.id, lk.name, lk.description, lkdh.quantity\n" +
                "from tbl_linh_kien_don_hang lkdh\n" +
                "left join tbl_linh_kien lk on lk.id = lkdh.tblLinhKienId\n" +
                "where lkdh.tblDonHangId = " + String.format("'%s'",id);
        try (
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query)
        ) {
            while(rs.next()){
                //Linh kien
                LinhKien lk = new LinhKien();
                lk.setId(rs.getString("id"));
                lk.setName(rs.getString("name"));
                lk.setDescription(rs.getString("description"));

                // Linh kien don hang
                LinhKienDonHang lkdh = new LinhKienDonHang();
                lkdh.setQuantity(rs.getInt("quantity"));
                lkdh.setLinhKien(lk);
                lkdhs.add(lkdh);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lkdhs;
    }
}
