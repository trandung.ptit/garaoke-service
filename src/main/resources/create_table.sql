CREATE TABLE `tbl_don_hang` (
  `id` varchar(10) DEFAULT NULL,
  `createdDate` date DEFAULT NULL,
  `totalPrice` double DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `tblXeId` varchar(10) DEFAULT NULL,
  `tblKhachHangId` varchar(10) DEFAULT NULL,
  `tblPhuTrachKyThuatId` varchar(10) DEFAULT NULL,
  `tblNhanVienKhoId` varchar(10) DEFAULT NULL,
  `tblNhanVienKeToanId` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tbl_khach_hang` (
  `id` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;_NOTES */;