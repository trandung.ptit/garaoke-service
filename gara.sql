/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `tbl_don_hang`;
CREATE TABLE `tbl_don_hang` (
  `id` varchar(10) DEFAULT NULL,
  `createdDate` date DEFAULT NULL,
  `totalPrice` double DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `tblXeId` varchar(10) DEFAULT NULL,
  `tblKhachHangId` varchar(10) DEFAULT NULL,
  `tblPhuTrachKyThuatId` varchar(10) DEFAULT NULL,
  `tblNhanVienKhoId` varchar(10) DEFAULT NULL,
  `tblNhanVienKeToanId` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `tbl_khach_hang`;
CREATE TABLE `tbl_khach_hang` (
  `id` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `tbl_linh_kien`;
CREATE TABLE `tbl_linh_kien` (
  `id` varchar(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `tblKhoId` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `tbl_linh_kien_don_hang`;
CREATE TABLE `tbl_linh_kien_don_hang` (
  `id` varchar(10) NOT NULL,
  `price` double DEFAULT NULL,
  `tblLinhKienId` varchar(10) DEFAULT NULL,
  `tblDonHangId` varchar(10) DEFAULT NULL,
  `quantity` int(10) DEFAULT NULL,
  `totalPrice` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tbl_don_hang` (`id`, `createdDate`, `totalPrice`, `status`, `note`, `tblXeId`, `tblKhachHangId`, `tblPhuTrachKyThuatId`, `tblNhanVienKhoId`, `tblNhanVienKeToanId`) VALUES
('DH33528', '2021-01-01', 8822049373, 'Đã tạo', '', 'XE187', 'KH442', 'KYTHUAT758', 'KHO048', 'KETOAN150');
INSERT INTO `tbl_don_hang` (`id`, `createdDate`, `totalPrice`, `status`, `note`, `tblXeId`, `tblKhachHangId`, `tblPhuTrachKyThuatId`, `tblNhanVienKhoId`, `tblNhanVienKeToanId`) VALUES
('DH43150', '2021-01-01', 5934296264, 'Đã tạo', '', 'XE673', 'KH810', 'KYTHUAT016', 'KHO242', 'KETOAN488');
INSERT INTO `tbl_don_hang` (`id`, `createdDate`, `totalPrice`, `status`, `note`, `tblXeId`, `tblKhachHangId`, `tblPhuTrachKyThuatId`, `tblNhanVienKhoId`, `tblNhanVienKeToanId`) VALUES
('DH61633', '2021-01-01', 363525963, 'Đã tạo', '', 'XE813', 'KH062', 'KYTHUAT859', 'KHO314', 'KETOAN509');
INSERT INTO `tbl_don_hang` (`id`, `createdDate`, `totalPrice`, `status`, `note`, `tblXeId`, `tblKhachHangId`, `tblPhuTrachKyThuatId`, `tblNhanVienKhoId`, `tblNhanVienKeToanId`) VALUES
('DH35061', '2021-01-01', 8109971148, 'Đã tạo', '', 'XE584', 'KH442', 'KYTHUAT385', 'KHO441', 'KETOAN783'),
('DH15951', '2021-01-01', 3914837470, 'Đã tạo', '', 'XE067', 'KH767', 'KYTHUAT447', 'KHO670', 'KETOAN941'),
('DH43603', '2021-01-01', 9584478532, 'Đã tạo', '', 'XE752', 'KH781', 'KYTHUAT374', 'KHO277', 'KETOAN170'),
('DH29162', '2021-01-01', 3681635749, 'Đã tạo', '', 'XE149', 'KH062', 'KYTHUAT336', 'KHO295', 'KETOAN787'),
('DH93687', '2021-01-01', 5450075480, 'Đã tạo', '', 'XE020', 'KH781', 'KYTHUAT272', 'KHO011', 'KETOAN318'),
('DH93587', '2021-01-01', 3744033279, 'Đã tạo', '', 'XE125', 'KH482', 'KYTHUAT541', 'KHO970', 'KETOAN776'),
('DH01995', '2021-01-01', 3709678218, 'Đã tạo', '', 'XE927', 'KH527', 'KYTHUAT968', 'KHO905', 'KETOAN648'),
('DH92316', '2021-01-01', 6331495576, 'Đã tạo', '', 'XE412', 'KH767', 'KYTHUAT071', 'KHO889', 'KETOAN981'),
('DH23907', '2021-01-01', 3144425818, 'Đã tạo', '', 'XE012', 'KH245', 'KYTHUAT805', 'KHO591', 'KETOAN552'),
('DH51752', '2021-01-01', 6527659318, 'Đã tạo', '', 'XE334', 'KH442', 'KYTHUAT043', 'KHO728', 'KETOAN419'),
('DH52136', '2021-01-01', 7856546883, 'Đã tạo', '', 'XE649', 'KH781', 'KYTHUAT800', 'KHO501', 'KETOAN950'),
('DH19927', '2021-01-01', 8531154456, 'Đã tạo', '', 'XE172', 'KH062', 'KYTHUAT422', 'KHO921', 'KETOAN954'),
('DH51973', '2021-01-01', 6385831345, 'Đã tạo', '', 'XE641', 'KH527', 'KYTHUAT756', 'KHO184', 'KETOAN465'),
('DH61716', '2021-01-01', 6957929833, 'Đã tạo', '', 'XE118', 'KH873', 'KYTHUAT956', 'KHO461', 'KETOAN464'),
('DH53819', '2021-01-01', 1625408676, 'Đã tạo', '', 'XE465', 'KH914', 'KYTHUAT304', 'KHO589', 'KETOAN162'),
('DH09521', '2021-01-01', 2547865416, 'Đã tạo', '', 'XE008', 'KH062', 'KYTHUAT307', 'KHO693', 'KETOAN815'),
('DH98866', '2021-01-01', 8668612697, 'Đã tạo', '', 'XE743', 'KH781', 'KYTHUAT435', 'KHO217', 'KETOAN872');

INSERT INTO `tbl_khach_hang` (`id`, `name`, `phone`, `address`, `note`) VALUES
('KH767', 'Tuxill', '969-111-75', '818 Marquette Avenue', 'Polynesian');
INSERT INTO `tbl_khach_hang` (`id`, `name`, `phone`, `address`, `note`) VALUES
('KH175', 'Bidmead', '936-141-02', '572 Donald Terrace', '');
INSERT INTO `tbl_khach_hang` (`id`, `name`, `phone`, `address`, `note`) VALUES
('KH873', 'Haszard', '950-288-01', '7188 Meadow Vale Parkway', '');
INSERT INTO `tbl_khach_hang` (`id`, `name`, `phone`, `address`, `note`) VALUES
('KH605', 'Hallett', '504-378-90', '441 Clyde Gallagher Pass', ''),
('KH914', 'Sunley', '970-398-82', '34772 Clove Way', ''),
('KH442', 'Shorland', '910-681-71', '53715 Magdeline Point', ''),
('KH244', 'Sedgman', '534-646-75', '57733 Golf Course Street', ''),
('KH584', 'Esler', '789-546-59', '9157 Forest Plaza', 'Sioux'),
('KH482', 'Kaindl', '613-720-76', '4 Vernon Road', ''),
('KH810', 'Scola', '121-482-93', '0972 Westend Street', ''),
('KH062', 'Prescote', '627-662-15', '8 Charing Cross Hill', 'Alaskan Athabascan'),
('KH245', 'Lamberth', '664-373-46', '75 Sage Parkway', ''),
('KH781', 'Boughton', '390-575-41', '2 Dapin Circle', ''),
('KH112', 'Sprade', '396-119-58', '73 Pierstorff Alley', ''),
('KH527', 'Rolance', '478-759-24', '69 Nova Drive', '');

INSERT INTO `tbl_linh_kien` (`id`, `name`, `description`, `price`, `tblKhoId`) VALUES
('LK095', 'Bảng điều khiển điều hòa ', 'Hàng mới chính hãng', 745888, 'K9');
INSERT INTO `tbl_linh_kien` (`id`, `name`, `description`, `price`, `tblKhoId`) VALUES
('LK143', 'Bộ điều chỉnh trung tâm', 'Hàng mới chính hãng', 730724, 'K8');
INSERT INTO `tbl_linh_kien` (`id`, `name`, `description`, `price`, `tblKhoId`) VALUES
('LK154', 'Cảm biến ABS, trước', 'Hàng mới chính hãng', 935002, 'K0');
INSERT INTO `tbl_linh_kien` (`id`, `name`, `description`, `price`, `tblKhoId`) VALUES
('LK269', 'Bậc bước chân cửa trước phải', 'Hàng mới chính hãng', 546806, 'K0'),
('LK323', 'Bản lề cửa trước dưới bên phải', 'Hàng mới chính hãng', 773242, 'K7'),
('LK359', 'Bánh đà Mercedes', 'Hàng mới chính hãng', 552770, 'K6'),
('LK371', 'Bánh đà động cơ', 'Hàng mới chính hãng', 291736, 'K2'),
('LK372', 'Cảm biến chiều cao', 'Hàng mới chính hãng', 642235, 'K4'),
('LK404', 'Cảm biến ABS sau, trái', 'Hàng mới chính hãng', 343610, 'K6'),
('LK434', 'Bi tăng dây curoa', 'Hàng mới chính hãng', 779279, 'K2'),
('LK448', 'Ống xả', 'Hàng mới chính hãng', 752216, 'K1'),
('LK457', 'Bầu lọc pô đoạn giữa', 'Hàng mới chính hãng', 930649, 'K5'),
('LK461', 'Bình nước rửa kính BMW', 'Hàng mới chính hãng', 821752, 'K9'),
('LK474', 'Bầu lọc than hoạt tính', 'Hàng mới chính hãng', 574675, 'K0'),
('LK494', 'Má phanh trước', 'Hàng mới chính hãng', 825879, 'K4'),
('LK570', 'Bơm nước', 'Hàng mới chính hãng', 677541, 'K5'),
('LK572', 'Bi may ơ trước', 'Hàng mới chính hãng', 682860, 'K7'),
('LK580', 'Bộ làm mát dầu BMW', 'Hàng mới chính hãng', 165992, 'K6'),
('LK608', 'Bánh răng cam hút', 'Hàng mới chính hãng', 529835, 'K4'),
('LK659', 'Bầu trợ lực phanh', 'Hàng mới chính hãng', 509800, 'K8'),
('LK690', 'Cảm biến ABS sau, phải', 'Hàng mới chính hãng', 738015, 'K8'),
('LK751', 'Bình nước phụ BMW', 'Hàng mới chính hãng', 752512, 'K2'),
('LK823', 'Bình nước làm mát BMW', 'Hàng mới chính hãng', 207436, 'K7'),
('LK829', 'Bánh răng xích dưới động cơ', 'Hàng mới chính hãng', 725727, 'K6'),
('LK855', 'Babule trái Mercedes', 'Hàng mới chính hãng', 409769, 'K2'),
('LK883', 'Bộ Van chia dầu thủy lực thanh cân bằng', 'Hàng mới chính hãng', 246571, 'K6'),
('LK904', 'Bình ắc quy cho hệ thống lái BMW', 'Hàng mới chính hãng', 283414, 'K2'),
('LK966', 'Bệ tỳ tay cánh cửa trước phải', 'Hàng mới chính hãng', 251301, 'K4'),
('LK976', 'Cảm biến má phanh trước', 'Hàng mới chính hãng', 863175, 'K9'),
('LK978', 'Babule phải Mercedes', 'Hàng mới chính hãng', 731059, 'K0');

INSERT INTO `tbl_linh_kien_don_hang` (`id`, `price`, `tblLinhKienId`, `tblDonHangId`, `quantity`, `totalPrice`) VALUES
('LKDH01584', 279953, 'LK359', 'DH23907', 2, 42);
INSERT INTO `tbl_linh_kien_don_hang` (`id`, `price`, `tblLinhKienId`, `tblDonHangId`, `quantity`, `totalPrice`) VALUES
('LKDH02009', 728143, 'LK976', 'DH43150', 3, 4);
INSERT INTO `tbl_linh_kien_don_hang` (`id`, `price`, `tblLinhKienId`, `tblDonHangId`, `quantity`, `totalPrice`) VALUES
('LKDH02211', 938512, 'LK448', 'DH29162', 5, 42);
INSERT INTO `tbl_linh_kien_don_hang` (`id`, `price`, `tblLinhKienId`, `tblDonHangId`, `quantity`, `totalPrice`) VALUES
('LKDH04337', 663915, 'LK404', 'DH35061', 1, 56),
('LKDH21282', 967268, 'LK494', 'DH33528', 3, 12),
('LKDH22169', 221062, 'LK580', 'DH93587', 4, 18),
('LKDH27363', 791543, 'LK269', 'DH01995', 3, 63),
('LKDH28768', 695956, 'LK434', 'DH09521', 5, 92),
('LKDH28792', 609142, 'LK690', 'DH15951', 5, 86),
('LKDH30860', 302458, 'LK457', 'DH19927', 2, 33),
('LKDH37333', 171477, 'LK751', 'DH43603', 1, 33),
('LKDH37859', 559059, 'LK904', 'DH35061', 4, 78),
('LKDH47799', 939247, 'LK143', 'DH93687', 5, 73),
('LKDH51300', 720320, 'LK570', 'DH43603', 3, 98),
('LKDH54605', 103665, 'LK978', 'DH93687', 5, 33),
('LKDH56215', 263703, 'LK829', 'DH43150', 3, 11),
('LKDH62421', 866855, 'LK608', 'DH52136', 5, 64),
('LKDH64677', 284065, 'LK461', 'DH29162', 4, 28),
('LKDH65947', 166789, 'LK154', 'DH61633', 3, 16),
('LKDH68554', 465266, 'LK572', 'DH53819', 2, 5),
('LKDH68692', 218054, 'LK372', 'DH98866', 1, 93),
('LKDH71384', 557166, 'LK474', 'DH51973', 2, 85),
('LKDH71927', 746563, 'LK095', 'DH51752', 1, 68),
('LKDH75065', 884972, 'LK659', 'DH61716', 1, 34),
('LKDH75549', 410858, 'LK883', 'DH01995', 1, 10),
('LKDH75679', 111955, 'LK371', 'DH33528', 3, 12),
('LKDH82153', 424080, 'LK855', 'DH93587', 4, 66),
('LKDH83645', 651361, 'LK323', 'DH92316', 4, 99),
('LKDH96836', 843769, 'LK823', 'DH15951', 5, 34),
('LKDH97925', 461384, 'LK966', 'DH61633', 3, 23);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;